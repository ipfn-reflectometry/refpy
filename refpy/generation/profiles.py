"""This package includes several functions to generate simulated data
that can be used as source examples

"""

import numpy as np


def create_density_profile(radius, ne_max,
                            n=2, m=5,
                            edge_start=None, edge_stop=None):
    """ Creates a symmetric density profile at the given radial positions
    with central density ne_max.

    If the profile edges are provided, the profile location is calculated
    between these. Otherwise it calculates between the lower and upper
    limit of the radius


    Parameters
    ----------
    radius : array_like
        Radial positions for which to return density profile
    ne_max :
        Central maximum density value

    n, m : optional
        Shape of the density profile

    edge_start : optional
        Start position of the density profile
    edge_stop : optional
        Stop position of the density profile

    Returns
    -------
    radius :
        Radial positions of the generated density profile
    density :
        Density values at each radial position

    Notes
    -----
    The density profile follows the shape:

    .. math:: n_e(r) = n_{e_{max}}\left(1-\left(|r-r0|/|edge-r0|\right)^m\right)^n


    """
    radius = np.array(radius)

    if edge_start is None:
        edge_start = radius[0]
    if edge_stop is None:
        edge_stop = radius[-1]

    r_central = (edge_start+edge_stop)/2

    dens = ne_max * np.power(1.0 - np.power( np.abs(radius - r_central)/np.abs(r_central-edge_stop), m), n)
    dens[np.where(radius < min(edge_start,edge_stop))] = 0
    dens[np.where(radius > max(edge_start,edge_stop))] = 0
    density = np.array(dens)

    return radius, density



def create_magnetic_profile(radius,b0,r0=None,origin=0):
    """ Generates a magnetic field profile at the radial positions.
    Assumes the magnetic profile is a reciprocal function centered
    at geo_origin.


    Parameters
    ----------
    radius : array_like
        Radial positions for which to return density profile
    b0 :
        Magnetic field value at the geo_r position

    r0 : optional
        Radial position of the provided magnetic field.
        If not provided assumes central of radius values
    origin : optional
        Origin of the magnetic field source. Usually at r=0

    Returns
    -------
    radius :
        Radial positions of the generated magnetic profile
    bfield :
        Magnetic field profile

    Notes
    -----
    The magnetic profile follows the reciprocal shape centered at
    geo_origin:

    .. math:: B(r) = B_0\left(\frac{r0}{r-origin}\right)

    """
    radius = np.array(radius)

    if r0 is None:
        r0 = (radius[0]+radius[-1])/2

    bfield = np.abs(b0 * (r0)/(radius-origin))

    return radius, bfield
