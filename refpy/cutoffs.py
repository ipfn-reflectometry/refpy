#!/usr/bin/env python
"""cutoffs calculates the cutoff frequencies for different waves
propagating along a plasma with a given shape

The cutoff calculation can be used and an example can be run by
calling this function as

Examples
-------
$ python cutoffs.py --help
$ python cutoffs.py


Authors
-------
L. Guimarais
D. Aguiam

"""
__version__ = "1.0"

from common import calc_b2fce
from common import calc_ne2fpe


from generation import profiles

import numpy as np


def calc_cuttoff_xmode( density, bfield, region='upper'):
    """ Calculates the X-mode cut off frequencies for the given density
    and magnetic field values

    Parameters
    ----------
    density : array_like
        Density profile
    bfield : array_like
        Magnetic field value at the geo_r position

    region : {'upper','lower'} optional
        X-mode cut off region to return
        If not provided assumes central of radius values
    origin : optional
        Origin of the magnetic field source. Usually at r=0

    Returns
    -------
    cutoff :
        Cutoff frequencies for the region

    Notes
    -----
    The X-mode has two cut offs, upper and lower,

    .. math:: f_{uc}(r) = \sqrt{\frac{f_{ce}^2}{4} + f_{pe}^2} + \frac{f_{ce}}{2}

    .. math:: f_{lc}(r) = \sqrt{\frac{f_{ce}^2}{4} + f_{pe}^2} - \frac{f_{ce}}{2}

    """

    fce = calc_b2fce(bfield)
    fpe = calc_ne2fpe(density)


    assert region in ['upper','u','uc','lower','l','lc'], "Region must be 'upper' or 'lower' "

    cutoff = np.array([])
    if region in ['upper','u','uc']:
        cutoff = np.sqrt( np.power( fce/2, 2) + np.power(fpe, 2) ) + fce/2

    if region in ['lower','l','lc']:
        cutoff = np.sqrt( np.power( fce/2, 2) + np.power(fpe, 2) ) - fce/2

    return cutoff


def calc_cuttoff_omode(density):
    """ Calculates the O-mode cut off for the given density

    Parameters
    ----------
    density : array_like
        Density profile

    Returns
    -------
    cutoff :
        Cutoff frequencies for the region

    Notes
    -----
    The O-mode cut off frequencies are equal to the plasma frequency

    .. math:: f_{c}(r) = f_{pe}(ne(r))

    """
    fpe = calc_ne2fpe(density)
    cutoff = fpe
    return cutoff




if __name__ == "__main__":
    import argparse

    # Parses the arguments passed to the script
    parser = argparse.ArgumentParser(
            description='Plots cut off frequencies for different propagation modes',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            )

    parser.add_argument('-s','--shape',
                        help='Shape of the density profile [m n]',
                        default=[2,5],
                        nargs=2,
                        type=float)
    parser.add_argument('-ne','--density',
                        help='Core electron density of the plasma in m-3',
                        default=5e19,
                        type=float)
    parser.add_argument('-edges','--edges',
                        help='Start and stop edges of the density profile in m. Rgeo is calculated from middle point',
                        default=[1.07,2.23],
                        nargs=2,
                        type=float)

    parser.add_argument('-b','--bfield',
                        help='Magnetic field in core in T',
                        default=2.5,
                        type=float)

    parser.add_argument('-r','--radius',
                        help='Calculate cut offs between these radial positions',
                        default=[1, 2.3],
                        nargs=2,
                        type=float)

    parser.add_argument('-pts','--points',
                        help='Number of points along profile',
                        default=200,
                        type=int)


    args = parser.parse_args()



    example(density=args.density,
            bfield=args.bfield,
            radius=args.radius,
            points=args.points,
            shape=args.shape,
            edges=args.edges
            )

    m, n = args.shape
    ne_core = args.density
    edge_start,edge_stop = args.edges
    Bzero = args.bfield
    print Bzero
    print ne_core

    # Radial limits to interpolate
    rlims = args.radius
    #Number of points to interpolate
    npts = args.points

    #Get some machine coordinates
    Rgeo = (edge_start+edge_stop)/2

    #Radial position
    radius = np.linspace(rlims[0], rlims[1], npts)


    radius, bfield = profiles.create_magnetic_profile(radius,Bzero,r0=Rgeo)
    radius, density = profiles.create_density_profile(radius,ne_core,
                                        edge_start=edge_start,
                                        edge_stop=edge_stop)

    Fuc = calc_cuttoff_xmode(density, bfield, region='upper')
    Flc = calc_cuttoff_xmode(density, bfield, region='lower')
    Fpe = calc_ne2fpe(density)
    Fce = calc_b2fce(bfield)
    Fco = calc_cuttoff_omode(density)


    plt.grid()
    plt.xlabel('Radius [m]')
    plt.ylabel('Frequency [GHz]')
    titlestr = 'Density=' + str(ne_core) + '[m-3] Bt=' + str(Bzero) + '[T] m=' + str(m) + ' n=' + str(n)

    titlestr = 'Density %0.1g [m-3] B %0.1f [T] '%(ne_core,Bzero)
    plt.title(titlestr)
    plt.plot(radius, Fce*1e-9, label='Cyclotronic',linestyle='--')
    plt.plot(radius, Fco*1e-9, label='Plasma (O-mode)')
    plt.plot(radius, Fuc*1e-9, label='Upper (X-mode)')
    plt.plot(radius, Flc*1e-9, label='Lower (X-Mode)')
    plt.legend(fontsize='x-small')
    plt.show()
