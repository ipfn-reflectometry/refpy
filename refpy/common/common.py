""" Common Python functions and tools that may be used in other packages

"""
from scipy import constants as konst
import numpy as np


def closest_index(array,value):
    """ Finds the index of the array element closest to value

    Parameters
    ----------
    array : array_like
        Array to search in
    value :
        Value to close to in array

    Returns
    -------
    index : int
        Index of the value in the array closest to the provided value

    """
    return (np.abs(np.array(array)-value)).argmin()
