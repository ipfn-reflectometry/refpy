""" Physics functions used in the other packages

"""
from scipy import constants as konst
import numpy as np


def calc_ne2fpe(ne):
    r""" Calculates electron plasma frequency for the provided electron
    density array

    Parameters
    ----------
    ne : array_like
        Electron density array

    Returns
    -------
    fpe : ndarray
        Plasma electron frequency

    Notes
    -----
    .. math:: f_{pe} = \sqrt{\frac{n_e e^2}{4\pi^2\epsilon_0m_e}}

    """
    ne = np.abs(np.array(ne))
    assert np.all(ne>=0), 'Electron density is below 0'
    k1a = 4*np.power(konst.pi,2) * konst.epsilon_0  / np.power(konst.e,2)
    fpe = np.sqrt(ne/(k1a * konst.m_e))
    return fpe


def calc_fpe2ne(fpe):
    r""" Calculates electron density from electron plasma frequency

    Parameters
    ----------

    fpe : array_like
        Plasma electron frequency

    Returns
    -------
    ne : ndarray
        Electron density array

    Notes
    -----
    .. math:: f_{pe} = \sqrt{\frac{n_e e^2}{4\pi^2\epsilon_0m_e}}

    """
    fpe = np.abs(np.array(fpe))
    assert np.all(fpe>=0), 'Plasma electron frequency is below 0'
    k1a = 4*np.power(konst.pi,2) * konst.epsilon_0  / np.power(konst.e,2)
    ne = k1a * konst.m_e*np.power(fpe,2)
    return ne


def calc_b2fce(b):
    r""" Calculates electron cyclotron frequency from magnetic field

    Parameters
    ----------
    b : array_like
        Magnetic field

    Returns
    -------
    fce : ndarray
        Cyclotron frequency

    Notes
    -----
    .. math:: f_{ce} = \left | \frac{eB}{2\pi^2m_e} \right |

    """
    b = np.array(b)
    return np.abs(konst.e*np.array(b)/(2*konst.pi*konst.m_e))



def calc_fce2b(fce):
    r""" Calculates magnetic field from the provided electron
        cyclotron frequency

    Parameters
    ----------
    fce : array_like
        Cyclotron frequency

    Returns
    -------
    b : ndarray
        Magnetic field

    Notes
    -----
    .. math:: B =  \frac{2\pi^2m_ef_ce}{e} \right |

    """
    fce = np.array(fce)
    fce = np.abs(fce)
    return (fce*2*konst.pi*konst.m_e)/(konst.e)
