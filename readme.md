refpy
====

refpy is a set of microwave reflectometry data analysis tools developed in Python.
These tools are mainly used by the [IPFN](https://www.ipfn.tecnico.ulisboa.pt/) Microwave Reflectometry group.

 - Some of these tools assumed you have access to ASDEX Upgrade database and Libraries


Getting started
----

 User refpy library installation

```

$ pip install git+https://gitlab.com/ipfn-reflectometry/refpy.git

# Installation of ipfnpytools in addition to refpy
$ pip install git+https://gitlab.com/ipfn-reflectometry/ipfnpytools.git
$ cutoffs.py


```

Development setup

```
$ git clone https://gitlab.com/ipfn-reflectometry/refpy.git
$ cd refpy
$ python setup.py install --user

$ python examples/cutoffs.py

```


Uninstall

```
$ pip uninstall refpy
```



Contributing
----
refpy happily accepts contributions from interested users!
Please see our [contribution guidelines](CONTRIBUTING.md).



License
----
refpy is made available under the [GNU GPLv3 license](LICENSE.md).


Contributors
----
- @daguiam ([D. Aguiam](mailto:daguiam@ipfn.tecnico.ulisboa.pt) - Repository maintainer)
- @zarpedes (L. Guimarais)
-  (L. Gil)
- @egor.seliunin (E. Seliunin)
- @jos_ipfn (J. Santos)
- @IPFN-Asilva (A. Silva)
