import os
from setuptools import setup
import setuptools

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "refpy",
    version = "0.0.2",
    author = "IPFN Reflectometry group",
    author_email = "daguiam@ipfn.tecnico.ulisboa.pt",
    description = ("refpy microwave reflectometry data analysis tools"),
    license = "GPLv3",
    keywords = "microwave, reflectometry, data analysis",
    url = "",
    packages=['refpy', 'tests','refpy.common','refpy.generation'],
    #packages=setuptools.find_packages(),
    #ong_description=read('README.md'),
    classifiers=[
        "Development Status :: alpha ",
        "Topic :: Data analysis",
        "License :: GPLv3 license",
    ],
    install_requires=[
          'numpy','scipy',
    ],
    #scripts=['scripts/cutoffs.py']
)
