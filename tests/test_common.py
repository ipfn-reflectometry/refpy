import unittest
#import refpy.common as common
#import refpy.physics as physics

#from refpy import common

from refpy.common import *


class TestCommon(unittest.TestCase):
    """
    """

    def test_closest_idx(self):
        self.assertEqual(closest_index([1,2,3,4,5],2.6),2)


class TestPhysics(unittest.TestCase):
    """
    """

    
    def test_calc_b2fce_negative_values(self):
        self.assertEqual(calc_b2fce([-2.5]),calc_b2fce([2.5]))
    def test_calc_b2fce_zero_values(self):
        self.assertEqual(calc_b2fce([0]),0)
    def test_calc_b2fce_typeError(self):
        self.assertRaises(TypeError,calc_b2fce,[222,'stringvalue'])

    def test_calc_fce2b_negative_values(self):
        self.assertEqual(calc_fce2b([-2.5]),calc_fce2b([2.5]))
    def test_calc_fce2b_zero_values(self):
        self.assertEqual(calc_fce2b([0]),0)
    def test_calc_fce2b_typeError(self):
        self.assertRaises(TypeError,calc_fce2b,[222,'stringvalue'])


    def test_calc_ne2fpe_negative_values(self):
        self.assertEqual(calc_ne2fpe([-2.5]),calc_ne2fpe([2.5]))
    def test_calc_ne2fpe_zero_values(self):
        self.assertEqual(calc_ne2fpe([0]),0)
    def test_calc_ne2fpe_typeError(self):
        self.assertRaises(TypeError,calc_ne2fpe,[222,'stringvalue'])

    def test_calc_fpe2ne_negative_values(self):
        self.assertEqual(calc_fpe2ne([-2.5]),calc_fpe2ne([2.5]))
    def test_calc_fpe2ne_zero_values(self):
        self.assertEqual(calc_fpe2ne([0]),0)
    def test_calc_fpe2ne_typeError(self):
        self.assertRaises(TypeError,calc_fpe2ne,[222,'stringvalue'])



#    def test_closest_idx(self):
#        self.assertEqual(refpy.common.closest_index([1,2,3,4,5],2.6),2)
#
#    def test_closest_idx(self):
#        self.assertEqual(refpy.common.closest_index([1,2,3,4,5],2.6),2)





if __name__ == '__main__':
    pass
